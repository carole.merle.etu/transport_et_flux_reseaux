# Transport et flux réseaux PARTIE 2

### Reverse ssh tunnel

**Objectif** : Se connecter à distance via ssh sur le poste de travail qui est à l'interieur du réseau de l'entreprise.  
**Problème** : Le port 22 est bloqué pour les connexions entrantes.   
**Solution** : Reverse ssh tunnel.  

#### Pour mettre en place :

1. Installer autossh & ssh (normalement déjà installé sur les postes de travail)  

> sudo apt-get install autossh ssh

*autossh: pour garder la connexion ouverte.*

2. Générer une pair de clés (si on a pas encore de clés, mais normalement il y en a déjà sur le poste de travail)  

> ssh-keygen

3. Copier la clé publique sur le PC personnel 

> ssh-copy-id -i .ssh/id_rsa.pub user@my_home_ip

4. **Sur le poste de travail**

> autossh -M 12345 -o "PubkeyAuthentication=yes" -o "PasswordAuthentication=no" -i ~/.ssh/id_rsa -R 12345:localhost:22 user@my_home_ip  

5. **Sur le PC à la maison**

> ssh -p 12345 user@127.0.0.1


### Comment empêcher les employés d'utiliser un tunnel ssh pour se connecter à distance sur le rêseau de l'entreprise












